import re
import yaml
import time


manager = ["backend_engineering_manager", "frontend_engineering_manager"]
user = ["pm", "pmm", "cm", "sets", "pdm", "ux", "uxr", "tech_writer", "tw_backup", "appsec_engineer"]



# The "gitlab_username" function is used to parse the team.yaml file
# and create a list of all users name to be used later against the users name we collect
# from data_stage.yam file.


def gitlab_username(path):
	with open(path, 'r', encoding='utf-8') as f:
	    doc = yaml.load(f)
	    for each in doc:
	    	List_name.append(each["name"])


# This function is used to parse the group and do the necessary editing as per
# the assignment requirements.


def parse_group(path):
	Line = 0
	with open(path, "r") as file:
		for each in file:
			if Line == 0 :
				Line += 1 
				continue
			else:
				each = each.split(",")
				re_pattern1 = r'\b-shared-infra.*?\b'
				re_pattern2 = r'\b-shared-services.*?\b'
				if re.findall(re_pattern1, each[3]):
					op = re.sub(re_pattern1, r'', each[3])
					List["group_name =>" + " " + op] = [{"manager_group =>": []}, {"users_group =>": []}]
				elif re.findall(re_pattern2, each[3]):
					pass
				else:
					List["group_name =>" + " " + each[3]] = [{"manager_group =>": []}, {"users_group =>": []}]
				Line +=1


# This function is add the parse the users from stage file and compare them against
# the user we collect from team and only append the one that existing on both files



def parse_users(path):

	with open(path, 'r') as f:
	    doc = yaml.load(f)

	    for each in doc["stages"]['manage']["groups"]:
	        each = each.casefold()
	        for i in List:
	            if each in i.split("-"):

	                for value in doc["stages"]['manage']["groups"][each]:
	                    if value in manager:
	                        if doc["stages"]['manage']["groups"][each][value] != "TBD":
	                            List[i][0]['manager_group =>'].append("member_name =>" + " " +(doc["stages"]['manage']["groups"][each][value]))


	                    if value in user:
	                        if isinstance((doc["stages"]['manage']["groups"][each][value]), list):
	                            for j in doc["stages"]['manage']["groups"][each][value]:
	                                if (j != "TBD" and j in List_name):
	                                    List[i][1]['users_group =>'].append([{"member_name =>":j},{"member_handler": "File formatting error prevent to parse gitlab's key content"}])
	                        elif (doc["stages"]['manage']["groups"][each][value] != "TBD" and doc["stages"]['manage']["groups"][each][value] in List_name):
	                            List[i][1]['users_group =>'].append([{"member_name =>": (doc["stages"]['manage']["groups"][each][value])}, {"member_handler": "File formatting error prevent to parse gitlab's key content"}])


# This function to write output of the process above into yaml file for further processing.
def write_output(List):
	out_file = open('users.yaml', "w")
	out_file.write(yaml.dump(List, default_flow_style=False, allow_unicode=False))
	out_file.close()                        



List = {}
List_name = []
manager_group = []
users_group = []
# The file path of groupes.
path = "C:\\Users\\rawads\\Desktop\\team.yml"
# The file path of  users.
path1 = "C:\\Users\\rawads\\Desktop\\department.csv"
# the file path of gitlab users
path2= "C:\\Users\\rawads\\Desktop\\data_stages.yml"



if __name__ == "__main__":
	gitlab_username(path)
	time.sleep(5)
	parse_group(path1)
	time.sleep(5)
	parse_users(path2)
	time.sleep(5)
	write_output(List)
	




